#include <Snake.h>

/*!
  @brief Constructor for Snake Object
  @param X Location of initial metapixel x
  @param Y Location of initial metapixel y
 */

Snake::Snake(int8_t X, int8_t Y){
	this->head_x = X;
	this->head_y = Y;
	this->dir    = 3;  //DEFAULT = 3 = RIGHT
	this->speed  = 400; //DEFAULT = 400ms
	this->size   = 0;
	grow(X,Y);
}

Snake::~Snake(){
    //Destructor
}

void Snake::setDir(int8_t dir){
	this->dir = dir;  
}

int8_t Snake::getDir(){
	return this->dir;
}

int8_t Snake::getSpeed(){
	return this->speed;
}

int8_t Snake::getHead(int8_t dim){
	if(dim == 0)
		return this->bodyX.back();
	else
		return this->bodyY.back();
}

void Snake::grow(int8_t x,int8_t y){
	this->bodyX.push_back(x);
	this->bodyY.push_back(y);
}

void Snake::crawl(int8_t x,int8_t y){
	this->bodyX.erase(bodyX.begin());
	this->bodyY.erase(bodyY.begin());
	grow(x,y);
}

int8_t Snake::getSize(void){
	return this->bodyX.size();
}