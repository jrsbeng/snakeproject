#include <Arduino.h>

//Includes 
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

#include <Snake.h>

#include <time.h>

#define PIN_BL  27
#define PIN_CLK 18
#define PIN_DIN 23
#define PIN_DC  19
#define PIN_CE  5
#define PIN_RST 14

#define PIN_UP     33
#define PIN_RIGHT  32
#define PIN_LEFT   35
#define PIN_DOWN   34

#define _PIX_DEFAULT 1

#define UP    0
#define DOWN  1
#define LEFT  2
#define RIGHT 3

#define BUSY 0
#define WAIT 1

// Software SPI (slower updates, more flexible pin options):
// pin 7 - Serial clock out (SCLK)
// pin 6 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(PIN_CLK, PIN_DIN, PIN_DC, PIN_CE, PIN_RST);

int xMax = display.width()-1;
int yMax = display.height()-1;

int resPixel = 3;
int xPxMax = display.width()/resPixel;
int yPxMax = display.height()/resPixel;
int xCenter = xPxMax/2;
int yCenter = yPxMax/2;
bool isNewFood = true;
unsigned long previousMillis = 0; 

Snake kobra = Snake(xCenter,yCenter);

int iX = xCenter;
int8_t dirOld = kobra.getDir();
int8_t nextPos[2];
int8_t kbSize;
int8_t statusGame = BUSY;

struct Button {
  uint32_t numberKeyPresses;
  bool up;
  bool left;
  bool right;
  bool down;
};

Button button = {0, false,false,false,false};

void IRAM_ATTR isr_up() {
  button.numberKeyPresses += 1;
  if(button.up == false)
    button.up = true;
}

void IRAM_ATTR isr_left() {
  button.numberKeyPresses += 1;
  if(button.left == false)
    button.left = true;
}

void IRAM_ATTR isr_right() {
  button.numberKeyPresses += 1;
  if(button.right == false)
    button.right = true;
}

void IRAM_ATTR isr_down() {
  button.numberKeyPresses += 1;
  if(button.down == false)
    button.down = true;
}

void fillMetaPixel(int8_t x,int8_t y){
  display.fillRect(x*resPixel,y*resPixel,resPixel,resPixel,BLACK);
}

void printFood(){
  fillMetaPixel(kobra.food.x,kobra.food.y);
}

void walk(){
  kobra.crawl(nextPos[0],nextPos[1]);
}

void displaySnake(){
  for(int i=0;i<kobra.getSize();i++){
    fillMetaPixel(kobra.bodyX[i],kobra.bodyY[i]);
  } 
}

void growFoodCollision(){
  if(isNewFood == true){
    srand (time(NULL));
    isNewFood = false;
    //Sorteia a nova isca
    kobra.food.x = random(xPxMax); 
    kobra.food.y = random(yPxMax);
  }
}

void drawWall(){
    display.drawLine(0,       0, xMax, 0, BLACK);
    display.drawLine(xMax, yMax, xMax, 0, BLACK);
    display.drawLine(xMax, yMax,    0, yMax, BLACK);
    display.drawLine(0,       0,    0, yMax, BLACK);
}

void updateDir(){
   if(dirOld != kobra.getDir()){
        switch(kobra.getDir()) {
          case UP :
              if(dirOld == DOWN){
                kobra.setDir(DOWN); //Does not affect
              }
              break; //optional
          case DOWN :
              if(dirOld == UP){
                kobra.setDir(UP); //Does not affect
              }
              break; //optional
          case LEFT :
              if(dirOld == RIGHT){
                kobra.setDir(LEFT); //Does not affect
              }
              break; //optional
          case RIGHT :
              if(dirOld == LEFT){
                kobra.setDir(RIGHT); //Does not affect
              }
              break; //optional            
          default:
              break; //optional            
        }
    }
}

void computeNextPosition(){
  kbSize = kobra.getSize()-1;
  switch(kobra.getDir()) {
      case UP :
          nextPos[0] = kobra.getHead(0);
          nextPos[1] = kobra.getHead(1) - 1;            
          if(nextPos[1]<0)
            nextPos[1] = yPxMax-1;
          break; //optional
      case DOWN :
          nextPos[0] = kobra.getHead(0);
          nextPos[1] = kobra.getHead(1) + 1;
          if(nextPos[1] > yPxMax-1)
            nextPos[1] = 0;
          break; //optional
      case LEFT :
          nextPos[0] = kobra.getHead(0)-1;
          nextPos[1] = kobra.getHead(1);
          if(nextPos[0] < 0)
            nextPos[0] = xPxMax-1;            
          break; //optional
      case RIGHT :
          nextPos[0] = kobra.getHead(0)+1;
          nextPos[1] = kobra.getHead(1);
          if(nextPos[0] > xPxMax-1)
            nextPos[0] = 0;            
          break; //optional            
      default:
          break; //optional            
    }
}

void checkCollision(){
  if((kobra.getHead(0)==kobra.food.x)&&(kobra.getHead(1)==kobra.food.y)){
      kobra.grow(kobra.food.x,kobra.food.y);
      isNewFood = true;
   }
}

void handleJoy(){
  if(button.up){
    button = {0, false,false,false,false};
    dirOld = kobra.getDir(); //Possível direção antiga
    kobra.setDir(UP); ////Novo candidato a direção
  } else
  if(button.left){
    button = {0, false,false,false,false};
    dirOld = kobra.getDir(); //Possível direção antiga
    kobra.setDir(LEFT); ////Novo candidato a direção
  }else
  if(button.right){
    button = {0, false,false,false,false};
    dirOld = kobra.getDir(); //Possível direção antiga
    kobra.setDir(RIGHT); ////Novo candidato a direção
  }else
  if(button.down){
    button = {0, false,false,false,false};
    dirOld = kobra.getDir(); //Possível direção antiga
    kobra.setDir(DOWN); ////Novo candidato a direção
  }
}

void setup() {
  
  pinMode(PIN_BL,OUTPUT);
  digitalWrite(PIN_BL,HIGH);

  pinMode(PIN_UP,     INPUT_PULLUP);
  pinMode(PIN_LEFT,   INPUT_PULLUP);
  pinMode(PIN_RIGHT,  INPUT_PULLUP);
  pinMode(PIN_DOWN,   INPUT_PULLUP);
  
  attachInterrupt(PIN_UP,    isr_up,    FALLING);
  attachInterrupt(PIN_LEFT,  isr_left,  FALLING);
  attachInterrupt(PIN_RIGHT, isr_right, FALLING);
  attachInterrupt(PIN_DOWN,  isr_down,  FALLING);
  
  
  display.begin();
  display.setContrast(50);
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.println("Welcome to the Snake Game!");
  display.display(); //splash screen
  /* initialize random seed: */
  randomSeed(analogRead(0));
  delay(1000);
}

void loop() {

    //01 - Mostra a tela
    if(statusGame == BUSY){
          
      previousMillis = millis();
      display.clearDisplay();
      
      //01-a-Mostra a Cobra
      displaySnake();
       
      //01-a-Mostra a Comida
  
      //02 - Computa Movimento
      
      //03 - Verifica Colisão
  
      //04 - Atualiza Cobra (Calcula a proxima cena)

      growFoodCollision();

      printFood();
      //Verifica onde tá a cabeça da cobra e calcula qual sera a proxima posição
  
      computeNextPosition();

       //Check if it grows (Collision/Food)
       checkCollision();

       //Walk
       walk();
      
      display.display(); //splash screen  
  
      //CRAWL        
      handleJoy();

      updateDir(); // Valida direção
      
      statusGame = WAIT;
  }
  else{
    if(millis() - previousMillis > 120){
      statusGame = BUSY;
    }
  }
}
